package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class BrandonReid_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> jimiHendrixTracks = new ArrayList<Song>();
    JimiHendrix jimiHendrix = new JimiHendrix();
	
    jimiHendrixTracks = jimiHendrix.getJimiHendrixSongs();
	
	playlist.add(jimiHendrixTracks.get(0));
	playlist.add(jimiHendrixTracks.get(1));
	
    KanyeWest KanyeWestBand = new KanyeWest();
	ArrayList<Song> KanyeWestTracks = new ArrayList<Song>();
    KanyeWestTracks = KanyeWestBand.getKanyeWestSongs();
	
	playlist.add(KanyeWestTracks.get(0));
	playlist.add(KanyeWestTracks.get(1));
	playlist.add(KanyeWestTracks.get(2));
	
    return playlist;
	}
}
