package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class Spencer {
	public LinkedList<PlayableSong> StudentPlaylist(){
		
		LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
		
		ArrayList<Song> imagineDragonsTracks = new ArrayList<Song>();
	    ImagineDragons imagineDragonsBand = new ImagineDragons();
	    imagineDragonsTracks = imagineDragonsBand.getImagineDragonsSongs();
		
		playlist.add(imagineDragonsTracks.get(0));
		playlist.add(imagineDragonsTracks.get(1));
		playlist.add(imagineDragonsTracks.get(2));
		
	    return playlist;
		}
	}

