package snhu.jukebox.playlist;

import snhu.student.playlists.*;

import java.util.ArrayList;
import java.util.List;

public class StudentList {

	public StudentList(){
	}

	public List<String> getStudentsNames() {
		ArrayList<String> studentNames = new ArrayList<String>();
		
		String StudentName1 = "TestStudent1Name";
		studentNames.add(StudentName1);
		
		String StudentName2 = "TestStudent2Name";
		studentNames.add(StudentName2);
		
		String StudentName3 = "Spencer";
		studentNames.add(StudentName3);

		//Adding Zach Muse
		String ZachMuse = "Zach Muse";
		studentNames.add(ZachMuse);
		
		String JohnMaddox = "John Maddox";
		studentNames.add(JohnMaddox);
		
		String BrandonReid = "Brandon Reid";
		studentNames.add(BrandonReid);
		
		/* NewFeature_KToooley */
		String KevinTooley = "Kevin Tooley";
		studentNames.add(KevinTooley);
		/* NewFeature_KToooley END */
		
		return studentNames;
	}

	public Student GetStudentProfile(String student){
		Student emptyStudent = null;
	
		switch(student) {
		   case "TestStudent1_Playlist":
			   TestStudent1_Playlist testStudent1Playlist = new TestStudent1_Playlist();
			   Student TestStudent1 = new Student("TestStudent1", testStudent1Playlist.StudentPlaylist());
			   return TestStudent1;
			   
		   case "TestStudent2_Playlist":
			   TestStudent2_Playlist testStudent2Playlist = new TestStudent2_Playlist();
			   Student TestStudent2 = new Student("TestStudent2", testStudent2Playlist.StudentPlaylist());
			   return TestStudent2;
			
		   case "Spencer":
			   Spencer SpencerList = new Spencer();
			   Student TestStudent3 = new Student("Spencer", SpencerList.StudentPlaylist());
			   return TestStudent3;

		   case "ZachMuse_Playlist":
			   ZachMuse_Playlist ZachMusePlaylist = new ZachMuse_Playlist();
			   Student ZachMuse = new Student("ZachMuse", ZachMusePlaylist.StudentPlaylist());
			   return ZachMuse;
			      
		   case "KTooley_Playlist":
			   KTooley_Playlist kTooleyPlaylist = new KTooley_Playlist();
			   Student KTooley = new Student("Kevin Tooley", kTooleyPlaylist.StudentPlaylist());
			   return KTooley;

		   case "JohnMaddox_Playlist":
			   JohnMaddox_Playlist johnMaddoxPlaylist = new JohnMaddox_Playlist();
			   Student JohnMaddox = new Student("John Maddox", johnMaddoxPlaylist.StudentPlaylist());
			   return JohnMaddox;
		}
		return emptyStudent;
	}
}
