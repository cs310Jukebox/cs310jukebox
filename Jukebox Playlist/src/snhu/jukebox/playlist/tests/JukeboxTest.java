package snhu.jukebox.playlist.tests;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Test;
import music.artist.*;
import snhu.jukebox.playlist.Song;

public class JukeboxTest {

	@Test
	public void testGetBeatlesAlbumSize() throws NoSuchFieldException, SecurityException {
		 TheBeatles theBeatlesBand = new TheBeatles();
		 ArrayList<Song> beatlesTracks = new ArrayList<Song>();
		 beatlesTracks = theBeatlesBand.getBeatlesSongs();
		 assertEquals(4, beatlesTracks.size());
	}
	
	@Test
	public void testGetLedZeppelinAlbumSize() throws NoSuchFieldException, SecurityException {
		 LedZeppelin ledZeppelin = new LedZeppelin();
		 ArrayList<Song> ledZeppelinTracks = new ArrayList<Song>();
		 ledZeppelinTracks = ledZeppelin.getLedZeppelinSongs();
		 assertEquals(5, ledZeppelinTracks.size());
	}
		
	@Test
	public void testGetJimiHendrixAlbumSize() throws NoSuchFieldException, SecurityException {
		 JimiHendrix jimiHendrix = new JimiHendrix();
		 ArrayList<Song> jimiHendrixTracks = new ArrayList<Song>();
		 jimiHendrixTracks = jimiHendrix.getJimiHendrixSongs();
		 assertEquals(5, jimiHendrixTracks.size());
	}
	
	@Test
	public void testGetImagineDragonsAlbumSize() throws NoSuchFieldException, SecurityException {
		 ImagineDragons imagineDragons = new ImagineDragons();
		 ArrayList<Song> imagineDragonsTracks = new ArrayList<Song>();
		 imagineDragonsTracks = imagineDragons.getImagineDragonsSongs();
		 assertEquals(3, imagineDragonsTracks.size());
	}
	
	@Test
	public void testGetAdelesAlbumSize() throws NoSuchFieldException, SecurityException {
		 Adele adele = new Adele();
		 ArrayList<Song> adelesTracks = new ArrayList<Song>();
		 adelesTracks = adele.getAdelesSongs();
		 assertEquals(3, adelesTracks.size());
	}
	//Adding Grimes Tests- Zach Muse
	@Test
	public void testGetGrimesAlbumSize() throws NoSuchFieldException, SecurityException {
		 Grimes grimes = new Grimes();
		 ArrayList<Song> grimesTracks = new ArrayList<Song>();
		 grimesTracks = grimes.getGrimesSongs();
		 assertEquals(3, grimesTracks.size());
	}
	//Adding M83 Tests- Zach Muse
	@Test
	public void testGetM83AlbumSize() throws NoSuchFieldException, SecurityException {
		 M83 m83 = new M83();
		 ArrayList<Song> M83Tracks = new ArrayList<Song>();
		 M83Tracks = m83.getM83Songs();
		 assertEquals(3, M83Tracks.size());
	}
	@Test
	public void testGetDefLeppardAlbumSize() throws NoSuchFieldException, SecurityException {
		 DefLeppard defLeppard = new DefLeppard();
		 ArrayList<Song> defLeppardTracks = new ArrayList<Song>();
		 defLeppardTracks = defLeppard.getDefLeppardSongs();
		 assertEquals(2, defLeppardTracks.size());
	}
	/* NewFeature_KToooley */
	@Test
	public void testGetZacBrownBandAlbumSize() throws NoSuchFieldException, SecurityException {
		 ZacBrownBand zbb = new ZacBrownBand();
		 ArrayList<Song> zbbTracks = new ArrayList<Song>();
		 zbbTracks = zbb.getZacBrownBandSongs();
		 assertEquals(3, zbbTracks.size());
	}
	
	@Test
	public void testGetSisterHazelAlbumSize() throws NoSuchFieldException, SecurityException {
		SisterHazel sh = new SisterHazel();
		 ArrayList<Song> shTracks = new ArrayList<Song>();
		 shTracks = sh.getSisterHazelSongs();
		 assertEquals(2, shTracks.size());
	}
	/* NewFeature_KToooley END */
	
}
