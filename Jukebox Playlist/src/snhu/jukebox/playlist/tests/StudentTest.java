package snhu.jukebox.playlist.tests;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import snhu.jukebox.playlist.Student;
import snhu.jukebox.playlist.StudentList;
import snhu.student.playlists.*;


public class StudentTest {

	//Test the list of student names and ensure we get back the name value we expect at the correct/specific point in the list
	@Test
	public void testGetStudentNameList1() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("TestStudent1Name", studentNames.get(0));							//test case for pass/fail. We expect the first name to be TestStudent1Name. Remember arrays start their count at 0 not 1.
	}
	
	@Test
	public void testGetStudentNameList2() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("TestStudent2Name", studentNames.get(1));							//test case to see if the second value contains the name we expect
	}
	
	@Test
	public void Spencer() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertTrue(studentNames.contains("Spencer"));							//test case to see if the third value contains the name we expect
	}
	
	/* NewFeature_KToooley */
	@Test
	public void testGetStudentNameList3() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assert(studentNames.contains("Kevin Tooley"));								//test case for pass/fail. We expect the first name to be TestStudent1Name. Remember arrays start their count at 0 not 1.
	}
	/* NewFeature_KToooley END */
	
	//Adding Zach Muse Tests
	@Test
	public void testGetZachMuse() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertTrue(studentNames.contains("Zach Muse"));							//test case to see if the second value contains the name we expect
	}
	
	@Test
	public void testGetJohnMaddox() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertTrue(studentNames.contains("John Maddox"));							    //test case to see if the second value contains the name we expect
	}

	@Test
	public void BrandonReid() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertTrue(studentNames.contains("Brandon Reid"));							    //test case to see if the second value contains the name we expect
	}
	
	//Module 6 Test Case Area
	//Test each student profile to ensure it can be retrieved and accessed
	@Test
	public void testGetStudent1Profile() {
		TestStudent1_Playlist testStudent1Playlist = new TestStudent1_Playlist();						//instantiating the variable for a specific student
		Student TestStudent1 = new Student("TestStudent1", testStudent1Playlist.StudentPlaylist());		//creating populated student object
		assertEquals("TestStudent1", TestStudent1.getName());											//test case pass/fail line - did the name match what you expected?
	}
	
	@Test
	public void testGetStudent2Profile() {
		TestStudent2_Playlist testStudent2Playlist = new TestStudent2_Playlist();
		Student TestStudent2 = new Student("TestStudent2", testStudent2Playlist.StudentPlaylist());
		assertEquals("TestStudent2", TestStudent2.getName());
	}
	
	@Test
	public void testGetKTooleyProfile() {
		KTooley_Playlist kTooleyPlaylist = new KTooley_Playlist();
		Student KTooley = new Student("Kevin Tooley", kTooleyPlaylist.StudentPlaylist());
		assertEquals("Kevin Tooley", KTooley.getName());
	}
	
	@Test
	public void testGetJohnMaddoxProfile() {
		JohnMaddox_Playlist johnMaddoxPlaylist = new JohnMaddox_Playlist();
		Student JohnMaddox = new Student("John Maddox", johnMaddoxPlaylist.StudentPlaylist());
		assertEquals("John Maddox", JohnMaddox.getName());
	}
	
	@Test
	public void testGetBrandonReidProfile() {
		BrandonReid_Playlist BrandonReidPlaylist = new BrandonReid_Playlist();
		Student BrandonReid = new Student("Brandon Reid", BrandonReidPlaylist.StudentPlaylist());
		assertEquals("Brandon Reid", BrandonReid.getName());
	}
	
	
	@Test
	public void testGetZachMuseProfile() {
		ZachMuse_Playlist zachMusePlaylist = new ZachMuse_Playlist();
		Student ZachMuse = new Student("Zach Muse", zachMusePlaylist.StudentPlaylist());
		assertEquals("Zach Muse", ZachMuse.getName());
	}
}
