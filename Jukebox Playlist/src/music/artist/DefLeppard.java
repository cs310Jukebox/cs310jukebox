package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;


//Added Def Leppard because this jukebox needs more hair bands
public class DefLeppard {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public DefLeppard() {
}
    
    public ArrayList<Song> getDefLeppardSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   
    	 Song track1 = new Song("Bringin' On the Heartbreak", "Def Leppard");             
         Song track2 = new Song("Pour Some Sugar on Me", "Def Leppard");         
         this.albumTracks.add(track1);                                          
         this.albumTracks.add(track2);                                          
         return albumTracks;                                                    
    }
}