package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class LedZeppelin {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public LedZeppelin() {
    }
    
    public ArrayList<Song> getLedZeppelinSongs() {
    	
    	albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	Song track1 = new Song("Kashmir", "Led Zeppelin");                     //Create a song
    	Song track2 = new Song("Black Dog", "Led Zeppelin");                   //Create another song
    	Song track3 = new Song("Ramble On", "Led Zeppelin");
    	Song track4 = new Song("Dazed And Confused", "Led Zeppelin");
    	Song track5 = new Song("Rock And Roll","Led Zeppelin");
    	this.albumTracks.add(track1);                                          //Add the first song to song list for the Beatles
    	this.albumTracks.add(track2);                                          //Add the second song to song list for the Beatles 
    	this.albumTracks.add(track3);
    	this.albumTracks.add(track4);
    	this.albumTracks.add(track5);                                          //Add the second song to song list for the Beatles 
        return albumTracks;                                                    //Return the songs for the Beatles in the form of an ArrayList
    }
}
