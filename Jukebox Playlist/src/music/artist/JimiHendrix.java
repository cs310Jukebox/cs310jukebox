package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class JimiHendrix {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public JimiHendrix() {
    }
    
    public ArrayList<Song> getJimiHendrixSongs() {
    	
    	albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	Song track1 = new Song("Hey Joe", "Jimi Hendrix");                     //Create a song
    	Song track2 = new Song("Purple Haze", "Jimi Hendrix");                 //Create another song
    	Song track3 = new Song("Little Wing", "Jimi Hendrix");
    	Song track4 = new Song("Foxy Lady", "Jimi Hendrix");
    	Song track5 = new Song("Who Knows","Jimi Hendrix");
    	this.albumTracks.add(track1);                                          //Add the first song to song list for the Beatles
    	this.albumTracks.add(track2);                                          //Add the second song to song list for the Beatles 
    	this.albumTracks.add(track3);
    	this.albumTracks.add(track4);
    	this.albumTracks.add(track5);                                          //Add the second song to song list for the Beatles 
        return albumTracks;                                                    //Return the songs for the Beatles in the form of an ArrayList
    }
}
