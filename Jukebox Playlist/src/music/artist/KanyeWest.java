package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class KanyeWest {

	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public KanyeWest() {
    }
    
    public ArrayList<Song> getKanyeWestSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Touch the sky", "Kanye West");             //Create a song
         Song track2 = new Song("Stronger", "Kanye West");         //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for the Kanye West
         this.albumTracks.add(track2);                                          //Add the second song to song list for the Kanye West 
    	 Song track3 = new Song("Heartless", "Kanye West");             //Create a song
         Song track4 = new Song("Mercy", "Kanye West");         //Create another song
         this.albumTracks.add(track3);                                          //Add the third song to song list for the Kanye West
         this.albumTracks.add(track4);  
         Song track5 = new Song("Power", "Kanye West");         //Create another song
         this.albumTracks.add(track5);    
         
         return albumTracks;                                                    //Return the songs for Kanye West in the form of an ArrayList
    }
}
