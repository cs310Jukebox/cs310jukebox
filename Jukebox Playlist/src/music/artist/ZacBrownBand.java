package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class ZacBrownBand {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public ZacBrownBand() {
    }
    
    public ArrayList<Song> getZacBrownBandSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Toes", "Zac Brown Band");             			//Create a song
         Song track2 = new Song("Jump Right In", "Zac Brown Band");         	//Create another song
         Song track3 = new Song("Keep Me In Mind", "Zac Brown Band");         	//Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Zac Brown Band
         this.albumTracks.add(track2);                                          //Add the second song to song list for Zac Brown Band
         this.albumTracks.add(track3);                                          //Add the second song to song list for Zac Brown Band
         return albumTracks;                                                    //Return the songs for Zac Brown Band in the form of an ArrayList
    }

}
