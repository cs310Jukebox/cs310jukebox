package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class SisterHazel {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public SisterHazel() {
    }
    
    public ArrayList<Song> getSisterHazelSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                 	//Instantiate the album so we can populate it below
    	 Song track1 = new Song("All For You", "Sister Hazel");            		//Create a song
         Song track2 = new Song("Mandolin Moon", "Sister Hazel");        		//Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Sister Hazel
         this.albumTracks.add(track2);                                          //Add the second song to song list for Sister Hazel 
         return albumTracks;                                                    //Return the songs for Sister Hazel in the form of an ArrayList
    }

}
